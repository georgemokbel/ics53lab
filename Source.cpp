//============================================================================
// Name        : Source.cpp
// Author      : Garrett Hawes (ghawes@uci.edu), George Mokbel (gmokbel@uci.edu) & Dennis Long Tran (dennisnt@uci.edu)
// Version     : 0.1
// Copyright   : Your copyright notice
// Description : First Project Lab
//============================================================================

#include "Shell.h"


int main(int argc, char* argv[])
{
	Shell shell;	
	// argc counts the number of command line parameters, by default
	// argv[0] is the name of this specific file, just a heads up
	// incase you don't know what this means.
	if (argc > 1)
		shell.run(argv[1]);
	else
		shell.run();

	cin.get();
	return 0;
}
